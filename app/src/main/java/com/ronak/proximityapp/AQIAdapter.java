package com.ronak.proximityapp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class AQIAdapter extends RecyclerView.Adapter<AQIAdapter.ViewHolder> {

    private List <ModelClass> itemList;
    private Context context;
    private OnGenericRvItemClickListener listener;

    public AQIAdapter(List <ModelClass> itemList, Context context, OnGenericRvItemClickListener listener) {
        this.listener = listener;
        this.itemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.aqi_adapter,
                parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tvCityName.setText(itemList.get(holder.getAdapterPosition()).getCity());
        holder.tvAQI.setText(String.valueOf(itemList.get(holder.getAdapterPosition()).getAqi()));

        if (itemList.get(holder.getAdapterPosition()).getAqi() >= 0 && itemList.get(holder.getAdapterPosition()).getAqi() <= 50.00) {
            holder.cardView.setBackgroundColor(context.getColor(R.color.color_good));
        } else if (itemList.get(holder.getAdapterPosition()).getAqi() >= 50.01 && itemList.get(holder.getAdapterPosition()).getAqi() <= 100.00) {
            holder.cardView.setBackgroundColor(context.getColor(R.color.color_satisfactory));
        } else if (itemList.get(holder.getAdapterPosition()).getAqi() >= 100.01 && itemList.get(holder.getAdapterPosition()).getAqi() <= 200.00) {
            holder.cardView.setBackgroundColor(context.getColor(R.color.color_moderate));
        } else if (itemList.get(holder.getAdapterPosition()).getAqi() >= 200.01 && itemList.get(holder.getAdapterPosition()).getAqi() <= 300.00) {
            holder.cardView.setBackgroundColor(context.getColor(R.color.color_poor));
        } else if (itemList.get(holder.getAdapterPosition()).getAqi() >= 300.01 && itemList.get(holder.getAdapterPosition()).getAqi() <= 400.00) {
            holder.cardView.setBackgroundColor(context.getColor(R.color.color_very_poor));
        } else {
            holder.cardView.setBackgroundColor(context.getColor(R.color.color_severe));
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRvItemClick(itemList.get(holder.getAdapterPosition()).getCity(), holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvCityName;
        TextView tvAQI;
        CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            tvCityName = (TextView) itemView.findViewById(R.id.tvCityName);
            tvAQI = (TextView) itemView.findViewById(R.id.tvAQI);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
        }
    }
}
