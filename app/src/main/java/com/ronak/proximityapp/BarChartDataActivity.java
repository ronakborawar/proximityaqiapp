package com.ronak.proximityapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.internal.Util;

public class BarChartDataActivity extends AppCompatActivity {

    private BarChart barChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_chart_data);

        Intent intent = getIntent();
        List<Float> listOfAQI = (List<Float>) intent.getSerializableExtra("list_of_aqi");

        barChart = (BarChart) findViewById(R.id.barChart);

        float max = listOfAQI.get(0);
        for (int i = 0; i < listOfAQI.size(); i++) {
            if (max < listOfAQI.get(i)) {
                max = listOfAQI.get(i);
            }
        }

        ArrayList<BarEntry> entries = new ArrayList<>();

        entries.add(new BarEntry(0, listOfAQI.get(0)));
        for (int i = 0; i < listOfAQI.size(); i++) {
            if (i % 5 == 0) {
                entries.add(new BarEntry(i, listOfAQI.get(i)));
            }
        }

        BarDataSet barDataSet;

        if (max >= 0 && max <= 50.00) {
            barDataSet = new BarDataSet(entries, "AQI is " + getString(R.string.good));
            barDataSet.setColors(ContextCompat.getColor(barChart.getContext(), R.color.color_good));
        } else if (max >= 50.01 && max <= 100.00) {
            barDataSet = new BarDataSet(entries, "AQI is " + getString(R.string.satisfactory));
            barDataSet.setColors(ContextCompat.getColor(barChart.getContext(), R.color.color_satisfactory));
        } else if (max >= 100.01 && max <= 200.00) {
            barDataSet = new BarDataSet(entries, "AQI is " + getString(R.string.moderate));
            barDataSet.setColors(ContextCompat.getColor(barChart.getContext(), R.color.color_moderate));
        } else if (max >= 200.01 && max <= 300.00) {
            barDataSet = new BarDataSet(entries, "AQI is " + getString(R.string.poor));
            barDataSet.setColors(ContextCompat.getColor(barChart.getContext(), R.color.color_poor));
        } else if (max >= 300.01 && max <= 400.00) {
            barDataSet = new BarDataSet(entries, "AQI is " + getString(R.string.very_poor));
            barDataSet.setColors(ContextCompat.getColor(barChart.getContext(), R.color.color_very_poor));
        } else {
            barDataSet = new BarDataSet(entries, "AQI is " + getString(R.string.severe));
            barDataSet.setColors(ContextCompat.getColor(barChart.getContext(), R.color.color_severe));
        }

        BarData theData = new BarData(barDataSet);
        barChart.setData(theData);
        barChart.getDescription().setText("Displayed every 5 second");
    }
}