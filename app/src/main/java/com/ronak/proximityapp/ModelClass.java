package com.ronak.proximityapp;

public class ModelClass {

    private String city;
    private double aqi;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getAqi() {
        return aqi;
    }

    public void setAqi(double aqi) {
        this.aqi = aqi;
    }

    public ModelClass (String city, double aqi) {
        this.city = city;
        this.aqi = aqi;
    }
}
