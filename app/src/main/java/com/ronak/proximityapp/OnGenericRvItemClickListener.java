package com.ronak.proximityapp;

import android.view.View;

public interface OnGenericRvItemClickListener {
    void onRvItemClick(String str, int position);
}
