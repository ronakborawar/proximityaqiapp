package com.ronak.proximityapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class MainActivity extends AppCompatActivity implements OnGenericRvItemClickListener {

    private OkHttpClient okHttpClient;

    private RecyclerView recyclerView;
    private List<ModelClass> modelClassList;
    private AQIAdapter adapter;
    private Context context;
    private HashMap <String,Double> graphData;
    private HashMap <String,List<Float>> listOfAQI;
    private OnGenericRvItemClickListener listener;
    private Set<String> citySet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        modelClassList = new ArrayList<>();
        listOfAQI = new HashMap<>();
        graphData = new HashMap<>();
        citySet = new LinkedHashSet<>();
        listener = this;
        adapter = new AQIAdapter(modelClassList, this, this);
        context = this;
        okHttpClient = new OkHttpClient();

        Request request = new Request.Builder().url("ws://city-ws.herokuapp.com/").build();
        EchoWebScoketListener listener = new EchoWebScoketListener();

        okHttpClient.newWebSocket(request, listener);

        okHttpClient.dispatcher().executorService().shutdown();
    }

    @Override
    public void onRvItemClick(String str, int position) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                for (Map.Entry<String, List<Float>> entry : listOfAQI.entrySet()) {
                    if (str.equalsIgnoreCase(entry.getKey())) {
                        Intent intent = new Intent(MainActivity.this, BarChartDataActivity.class);
                        intent.putExtra("list_of_aqi", (Serializable) entry.getValue());
                        startActivity(intent);
                    }
                }
            }
        });
    }

    private final class EchoWebScoketListener extends WebSocketListener {
        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);
            try {
                modelClassList = new ArrayList<>();
                try{
                    JSONArray array = new JSONArray(text);
                    final int length = array.length();
                    for(int i=0; i< length; i++) {
                        JSONObject obj = array.getJSONObject(i);
                        modelClassList.add(new ModelClass(obj.getString("city"), Double.valueOf(new DecimalFormat("####0.00").format(obj.getDouble("aqi")))));
                    }


                } catch (JSONException e) {
                    System.out.println(e);
                }

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        for (ModelClass modelClass : modelClassList) {

                            graphData.put(modelClass.getCity(), modelClass.getAqi());
                        }

                        for (Map.Entry<String,Double> entry : graphData.entrySet())
                        {
                            if (citySet.contains(entry.getKey())) {
                                List<Float> listValue = listOfAQI.get(entry.getKey());
                                listValue.add(entry.getValue().floatValue());
                                listOfAQI.put(entry.getKey(), listValue);
                            } else {
                                List<Float> listValue = new ArrayList<>();
                                listValue.add(entry.getValue().floatValue());
                                listOfAQI.put(entry.getKey(), listValue);
                            }
                            citySet.add(entry.getKey());
                        }

                        adapter = new AQIAdapter(modelClassList, context, listener);
                        GridLayoutManager manager = new GridLayoutManager(getBaseContext(), 3, GridLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(adapter);

                    }
                });

            } catch (Exception e) {
                System.out.println(e);
            }
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            super.onMessage(webSocket, bytes);
            webSocket.send(bytes);
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            super.onClosing(webSocket, code, reason);
            webSocket.close(1000, "Successfully Closed");
        }

        @Override
        public void onClosed(WebSocket webSocket, int code, String reason) {
            super.onClosed(webSocket, code, reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, @Nullable Response response) {
            super.onFailure(webSocket, t, response);
            webSocket.close(1000, "Failed to Close");
        }
    }
}